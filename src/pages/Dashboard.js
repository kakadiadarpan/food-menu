import React, { Component } from 'react';
import {Grid,Row,Col} from 'react-bootstrap';
import AppHeader from '../components/dashboard/AppHeader';
import CardContainer from '../containers/dashboard/CardContainer';

class Dashboard extends Component {

  render() {
    return (
      <Grid bsClass="">
        <div className="container-fluid">
          <AppHeader/>
          <CardContainer/>
        </div>
      </Grid>
    );
  }
}
export default Dashboard;
