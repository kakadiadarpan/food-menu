import axios from 'axios';

import DASHBOARD_CONSTANTS from '../constants/DashboardConstants';

export function getFoodItemsDataMapping() {
  const request = axios.get(`${DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING_URL}`, { headers: { 'Content-Type': 'application/json' } });
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING,
    payload: request
  };
}

export function getFoodItemsDataMappingSuccess(data) {
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING_SUCCESS,
    payload: data
  }
}

export function getFoodItemsDataMappingFailure(error) {
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING_FAILURE,
    payload: error
  }
}
