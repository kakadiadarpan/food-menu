import axios from 'axios';

import DASHBOARD_CONSTANTS from '../constants/DashboardConstants';

export function getFoodGroupData() {
  const request = axios.get(`${DASHBOARD_CONSTANTS.GET_FOOD_GROUP_DATA_URL}`, { headers: { 'Content-Type': 'application/json' } });
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_GROUP_DATA,
    payload: request
  };
}

export function getFoodGroupDataSuccess(data) {
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_GROUP_DATA_SUCCESS,
    payload: data
  }
}

export function getFoodGroupDataFailure(error) {
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_GROUP_DATA_FAILURE,
    payload: error
  }
}
