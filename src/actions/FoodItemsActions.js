import axios from 'axios';

import DASHBOARD_CONSTANTS from '../constants/DashboardConstants';

export function getFoodItemsData() {
  const request = axios.get(`${DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_URL}`, { headers: { 'Content-Type': 'application/json' } });
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA,
    payload: request
  };
}

export function getFoodItemsDataSuccess(data) {
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_SUCCESS,
    payload: data
  }
}

export function getFoodItemsDataFailure(error) {
  return {
    type: DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_FAILURE,
    payload: error
  }
}
