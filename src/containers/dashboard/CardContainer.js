import React from 'react';
import { connect } from 'react-redux';

import HeaderTabs from '../../components/dashboard/HeaderTabs';
import {
  getFoodGroupData,
  getFoodGroupDataSuccess,
  getFoodGroupDataFailure } from '../../actions/FoodGroupsActions';

import {
  getFoodItemsData,
  getFoodItemsDataSuccess,
  getFoodItemsDataFailure } from '../../actions/FoodItemsActions'

import {
  getFoodItemsDataMapping,
  getFoodItemsDataMappingSuccess,
  getFoodItemsDataMappingFailure } from '../../actions/FoodItemsMappingActions'

const mapDispatchToProps = (dispatch) => {
  return {
    getFoodGroupData : () => {
      dispatch(getFoodGroupData())
        .then((response) => {
          if(response.payload.status !== 200) {
            dispatch(getFoodGroupDataFailure(response.payload));
          } else {
            dispatch(getFoodGroupDataSuccess(response.payload));
          }
        });
    },
    getFoodItemsData : () => {
      dispatch(getFoodItemsData())
        .then((response) => {
          if(response.payload.status !== 200) {
            dispatch(getFoodItemsDataFailure(response.payload));
          } else {
            dispatch(getFoodItemsDataSuccess(response.payload));
          }
        });
    },
    getFoodItemsDataMapping : () => {
      dispatch(getFoodItemsDataMapping())
        .then((response) => {
          if(response.payload.status !== 200) {
            dispatch(getFoodItemsDataMappingFailure(response.payload));
          } else {
            dispatch(getFoodItemsDataMappingSuccess(response.payload));
          }
        });
    }
  }
};

function mapStateToProps(state) {
  return {
    foodGroups: state.foodGroups,
    foodItems: state.foodItems,
    foodMapping: state.foodMapping
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderTabs);
