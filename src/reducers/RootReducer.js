import { combineReducers } from 'redux';
import FoodGroupsReducer from './FoodGroupsReducer';
import FoodItemsReducer from './FoodItemsReducer';
import FoodItemsMappingReducer from './FoodItemsMappingReducer';

const rootReducer = combineReducers({
  foodGroups: FoodGroupsReducer,
  foodItems: FoodItemsReducer,
  foodMapping: FoodItemsMappingReducer
});

export default rootReducer;
