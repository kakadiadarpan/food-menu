import DASHBOARD_CONSTANTS from '../constants/DashboardConstants';

const INITIAL_STATE = {data:[], status: false, error: false, loading: false, type: false };

export default function(state = INITIAL_STATE, action) {
  let error;

  switch(action.type) {
    case DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING:
      return { ...state, status: 'fetching', error: null, loading: true, type: null };

    case DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING_SUCCESS:
      return { ...state, data: action.payload.data, status:'success', error: null, loading: false, type: null };

    case DASHBOARD_CONSTANTS.GET_FOOD_ITEMS_DATA_MAPPING_FAILURE:
      error = action.payload.data || {message: action.payload.message};
      return { ...state, data: [], status: 'failure', error: error, loading: false, type: null };

    default:
      return state;
  }
}
