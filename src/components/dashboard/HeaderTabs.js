import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import {Row,Col} from 'react-bootstrap';
import * as _ from 'lodash';
import FoodList from './FoodList';

export default class HeaderTabs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0
    };
  }

  componentDidMount() {
    this.props.getFoodGroupData();
    this.props.getFoodItemsData();
    this.props.getFoodItemsDataMapping();
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.foodGroups && nextProps.foodGroups.status == 'success') {
      this.setState({
        foodGroups:nextProps.foodGroups.data
      });
    }
    if(nextProps.foodItems && nextProps.foodItems.status == 'success') {
      this.setState({
        foodItems:nextProps.foodItems.data
      });
    }
    if(nextProps.foodMapping && nextProps.foodMapping.status == 'success') {
      this.setState({
        foodMapping:nextProps.foodMapping.data
      });
    }
  }

  getData(obj, i){
    let items = _.filter(this.state.foodItems,(item) =>{ return item.food_group_id===obj.food_group_id})
    return <FoodList food_items={items} key={i}/>
  }

  getTabs(obj, i){
    return <Tab className={(this && this.state && this.state.slideIndex)==i?"active":""} label={obj.food_group_name} data-value={obj.food_group_id} value={i} key={i} />
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  };

  render() {
    return (
      <Row className="body-wrapper">
        <Tabs
          className="tabs"
          onChange={this.handleChange}
          value={this.state.slideIndex}
        >
        {
          this.state.foodGroups && this.state.foodGroups.map(this.getTabs.bind(this))
        }
        </Tabs>
        <SwipeableViews
          props={this.props.children}
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
        {
          this.state.foodGroups && this.state.foodGroups.map(this.getData.bind(this))
        }
        </SwipeableViews>

      </Row>
    );
  }
}
