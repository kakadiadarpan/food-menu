import React from 'react';
import {Card, CardMedia, CardTitle} from 'material-ui/Card';
import {Row,Col} from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import AddShoppingCart from 'material-ui/svg-icons/action/add-shopping-cart';

class FoodList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="card-holder">
        {
          this.props && this.props.food_items && this.props.food_items.map(function(obj, i) {
            return (
              <Col xs={12} sm={6} md={4} lg={3} className="card" key={i}>
                <Card>
                  <CardMedia className="card-img" overlay={
                    <CardTitle title={obj.food_item_name} subtitle={obj.food_item_price?("$"+obj.food_item_price):("Small/Large:  $"+obj.food_item_price_s+"/$"+obj.food_item_price_l)}>
                    <IconButton className="add-cart"><AddShoppingCart color="white" /></IconButton>
                    </CardTitle>
                  }>
                  <img src={obj.food_item_pic} />
                  </CardMedia>
                </Card>
              </Col>
            )
          })
        }
      </div>
    )
  }
}

export default FoodList;
