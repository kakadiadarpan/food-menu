import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import AddShoppingCart from 'material-ui/svg-icons/action/add-shopping-cart';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginTop: '1%'
  },
  gridList: {
    width: "100%",
    overflowY: 'auto',
  },
  gridTile:{
    boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
    fontFamily: "Roboto, sans-serif"
  }
};

export default class FoodList extends React.Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div style={styles.root}>
        <GridList
          cellHeight={350}
          style={styles.gridList}
          cols={4}
          padding={10}
        >
        {this.props && this.props.food_items && this.props.food_items.map((item,i) => (
          <GridTile
            className={'grid-tile'}
            titleBackground={'rgba(0, 0, 0, 0.7)'}
            key={i}
            title={item.food_item_name}
            subtitle={<span>${item.food_item_price}</span>}
            actionIcon={<IconButton><AddShoppingCart color="white" /></IconButton>}
            style={styles.gridTile}
          >
            <img src={item.food_item_pic} />
          </GridTile>
        ))}
        </GridList>
      </div>
    )
  }
}
