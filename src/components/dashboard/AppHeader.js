import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import ActionShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import {Row,Col} from 'react-bootstrap';

const AppHeader = () => (
  <Row className="show-grid">
    <Col xs={12} className="header-wrapper">
      <AppBar
      title="Food Menu"
      iconElementRight={<IconButton><ActionShoppingCart/></IconButton>}
      titleStyle={{'textAlign':'center'}}
      />
    </Col>
  </Row>
);

export default AppHeader;
