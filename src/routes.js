import React from 'react';
import { Route, IndexRoute } from 'react-router';

// Importing all the required pages to Router
import App from './pages/App';
import Dashboard from './pages/Dashboard';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Dashboard} />
  </Route>
);
