var express  = require('express');
var app      = express();
var prodConfig = require('./configs/prodConfig'),
    proxy      = require('http-proxy-middleware'),
    options    = {
      target: 'http://ec2-54-165-240-14.compute-1.amazonaws.com:3000', // target host
      changeOrigin: true,               // needed for virtual hosted sites
      ws: true                         // proxy websockets
    },
    gatewayProxy = proxy(options);
app.use('/api', gatewayProxy);
app.use(express.static('dist'));
app.get(/^(?!api).*/, function(req, res) {
  res.sendFile(__dirname+'/src/index.html');
});
app.listen(prodConfig.PORT, prodConfig.HOST, function(err) {
  if(err) {
    console.log(err);
    return;
  }
  console.log('Listening at http://'+prodConfig.HOST+':'+prodConfig.PORT);
});
