var path      = require('path'),
    express   = require('express'),
    webpack   = require('webpack'),
    config    = require('./webpack.config.dev'),
    devConfig = require('./configs/devConfig'),
    app       = express(),
    compiler  = webpack(config),
    proxy     = require('http-proxy-middleware'),
    options   = {
        target: 'http://ec2-54-165-240-14.compute-1.amazonaws.com:3000', // target host
        changeOrigin: true,               // needed for virtual hosted sites
        ws: true                         // proxy websockets
    };
app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));
app.use(require('webpack-hot-middleware')(compiler));
// create the proxy (without context)
var gatewayProxy = proxy(options);
app.use('/api', gatewayProxy);
app.get(/^(?!api).*/, (req, res) => {
  res.sendFile(path.join(__dirname, './src/index.html'));
});
app.listen(devConfig.PORT, devConfig.HOST, (err) => {
  if(err) {
    console.log(err);
    return;
  }
  console.log('Listening at http://'+devConfig.HOST+':'+devConfig.PORT);
});
